# Making it more complex. Switching screens and placing buttons.
# We will be specifying locations for buttons.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Builder class.
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# Because we will be doing multiple screens, we need Screen and
# a manager to help arrange it.
# https://kivy.org/docs/api-kivy.uix.screenmanager.html
from kivy.uix.screenmanager import ScreenManager, Screen

# Since we are doing all the work in Kivy language below, we don't have to do
# anything in our classes. We just have to define them.
class MainScreen(Screen):
    pass

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class ThirdScreen(Screen):
    pass

class ScreenManagement(ScreenManager):
    pass

# The builder is going to load our Kivy language string. This will make it
# easier to define Kivy layouts.
# Having it all in one file helps when you need to get help from Kivy groups:
# https://github.com/kivy/kivy/wiki/Community-Guidelines
# Thus, these examples will use it from now on.
kv = Builder.load_string('''
# This defines our Root for Kivy. Because we don't define any movement types
# between screens, it will use the slide transition by default.
ScreenManagement:
    # Now attach all of the screens we are using to the root.
    MainScreen:
    FirstScreen:
    SecondScreen:
    ThirdScreen:

# This is our default start screen.
<MainScreen>:
    # The name allows us to reference it from buttons later.
    name: "main"
    # We are going to use a FloatLayout this time so we can explicitly
    # define button locations.
    # https://kivy.org/docs/api-kivy.uix.floatlayout.html
    FloatLayout:
        Button:
            # When clicked, we will switch to the screen named "first".
            on_release: app.root.current = "first"
            text: "First"
            font_size: 50
            # This will let the button scale based on the screen size.
            size_hint: 0.3,0.2
            # This will place the button on the top left of the screen.
            pos_hint: {"top":1}
        Button:
            on_release: app.root.current = "second"
            text: "Second"
            font_size: 50
            size_hint: 0.3,0.2
            # This will place the button starting on the top half of screen.
            pos_hint: {"top":.5}
        Button:
            on_release: app.root.current = "third"
            text: "Third"
            font_size: 50
            size_hint: 0.3,0.2
            # This will place the button on the bottom left of the screen.
            pos_hint: {"bottom":1}

<FirstScreen>:
    name: "first"
    Label:
        text_size: self.size
        halign: 'center'
        valign: 'middle'
        text: "First"
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Home"
        # When clicked, return to our "main" screen.
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}
           
<SecondScreen>:
    name: "second"
    Label:
        text_size: self.size
        halign: 'center'
        valign: 'middle'
        text: "Second"
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Home"
        # When clicked, return to our "main" screen.
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}

<ThirdScreen>:
    name: "third"
    Label:
        text_size: self.size
        halign: 'center'
        valign: 'middle'
        text: "Third"
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Home"
        # When clicked, return to our "main" screen.
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}
''')

class UseButtonApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        return kv

if __name__ == "__main__":
  UseButtonApp().run()
