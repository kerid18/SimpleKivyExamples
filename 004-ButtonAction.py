# Now to make the button so some action. Like update a string in a Label.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Button
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

# Import Builder class.
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# Import widget to attach Button to.
# https://kivy.org/docs/api-kivy.uix.widget.html
from kivy.uix.widget import Widget

# This allows for a changing string.
# https://kivy.org/docs/api-kivy.properties.html#kivy.properties.StringProperty
from kivy.properties import StringProperty

# We need a new class to reference our Kivy.
class KvButton(Widget):
    # In order for us to have a string that changes, we need to define it.
    # We are going to use StringProperty to set default. Kivy handles the rest.
    string = StringProperty("....")

    # Now a function to so the action.
    def update_str(self):
        # In this case we are only updating the string in self, which
        # references the Label created in the Kivy language below.
        self.string = 'World!'

# The builder is going to load our Kivy language string. This will make it
# easier to define Kivy layouts.
# Having it all in one file helps when you need to get help from Kivy groups:
# https://github.com/kivy/kivy/wiki/Community-Guidelines
# Thus, these examples will use it from now on.
kv = Builder.load_string('''
# Defining our root.
KvButton:

# Configuring our Screen.
<KvButton>:
    # This lets us post widgets into the screen without defining where every
    # button and label explicitly resides.
    BoxLayout:
        # Assigning a Button
        Button:
            # Defining Button text.
            text: "Hello"
            # Putting in center of screen.
            size: root.size
            on_release: root.update_str()
        # Creating a Label. By default, it just has what is in StringProperty
        Label:
            # It will get updates to it when referenced.
            text: root.string
''')

# This is your main class.
class UseButtonApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        # We have to return kv for the application to use it.
        return KvButton()

# Tell Python to start the app.
if __name__ == "__main__":
    # We are only going to run our class.
    UseButtonApp().run()
