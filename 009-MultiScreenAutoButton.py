# Massive thanks to spinningD20_ at #kivy IRC for helping me get this working
# as well as to all the others in that channel that helped at some point.
#
# This App generates buttons based on the list SomeList below. If you added a
# LOT of items to 007, the buttons got squished. Solve that with a scrollable
# view this time.

# This is the main import for Kivy.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Builder lets me pass the kv code in as a string instead of a separate kv file
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# ScreenManager helps me manage multiple screens. In this example, a home
# screen and a secondary templated screen. The FadeTransition is just the type
# of transition between the two screens.
# https://kivy.org/docs/api-kivy.uix.screenmanager.html
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

# The Factory object helps me programatically register widgets to add to Screen.
# https://kivy.org/docs/api-kivy.factory.html
from kivy.factory import Factory

# Need this for running at an explicit time.
# https://kivy.org/docs/api-kivy.clock.html
from kivy.properties import Clock

# Button class gives me a button to manipulate on the screen.
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

class ScreenManagement(ScreenManager):
    pass

class MainScreen(Screen):
    pass

class ViewScreen(Screen):
    # On initialization do this setup.
    def __init__(self, **kwargs):
        # Linking this to our ViewScreen.
        super(ViewScreen, self).__init__(**kwargs)
        # App is now how we can reference the Kivy code.
        app = App.get_running_app()
        # This clock schedules a refresh for generating the code at the very
        # first frame. Otherwise, we don't have enough setup in the App to
        # attach to and it dies.
        Clock.schedule_once(self.ready, 0)

    # Once the App is in ready state, build our buttons.
    def ready(self, *args):
        # Our manager is the App we are currently running as.
        # in 008, we referenced the Kivy code directly with this:
        # pbox = kv.ids.mainscreen.box
        # But now we have a manager to connect to once running.
        manager = App.get_running_app().manager
        rows = ["one", "2", "tres"]
        # Now loop through our list and generate a button.
        for item in rows:
            # Use our ScreenTemplate for what the buttons are to do.
            screen = Factory.ScreenTemplate(name=item)
            # Add that to our ScreenManager
            manager.add_widget(screen)
            # Creating a Button. Python lambda's are like mini functions. x is like
            # the parameter we pass to the function. Everything else is the work done
            # in the function. In this case we are matching the button item name to
            # the generated template screen that we defined a few lines above.
            # Have to define a height and y otherwise they just stack on top of each
            # other and isn't very useful.
            btn = Button(text=item, size_hint_y=None, height=40, on_press=lambda x: manager.setter('current')(x, x.text))
            # Now we add that button to the box as a widget.
            self.ids.box.add_widget(btn)

kv = Builder.load_string("""
#: import FadeTransition kivy.uix.screenmanager.FadeTransition

<MainScreen>:
    name: "main"
    GridLayout:
        cols: 1
        padding: 1
        spacing: 1
        size_hint_y: None
        height: self.minimum_height

        Label:
            text_size: self.size
            text: 'This button should take you to ViewScreen.\\n\
ViewScreen has buttons generated from list rows with via ScreenTemplate\\n\
Each button should be named after the list element.\\n\
The list generated button should take you to a screen with the name of\\n\
the button with a Home back to this page'
            halign: 'center'
            valign: 'middle'
            height: 150
            size_hint_y: None
        Button:
            font_size: 30
            height: 60
            size_hint_y: None
            text: "More Buttons ->"
            on_release: app.root.current = "view"

<ViewScreen>:
    name: "view"
    BoxLayout:
        orientation: 'vertical'
        ScrollView:
            do_scroll_x: False
            GridLayout:
                # This id links with the above property so we can reference it.
                id: box
                cols: 1
                padding: 1
                spacing: 1
                size_hint_y: None
                height: self.minimum_height
        Button:
            # Make it a different color because we can.
            color: 1,.5,1,1
            # Set the size
            font_size: 25
            # Setting how big of a button it should be
            height: 50
            size_hint_y: None
            # Set the text
            text: "Home"
            # Telling it to go back to the MainScreen
            on_release: app.root.current = "main"

<ScreenTemplate@Screen>:
    name: ""
    Label:
        text_size: self.size
        halign: 'center'
        valign: 'middle'
        text: root.name
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Home"
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}
""")

class SimpleAutoButtonsApp(App):
    def build(self):
        # Because we have lots of screens, we are going to use a manager.
        self.manager = ScreenManager()
        # Adding the Screens we will use to the manager.
        for screen in [ViewScreen(), MainScreen()]:
            self.manager.add_widget(screen)
        # Starting on the "main" screen.
        self.manager.current = 'main'
        # Use this new manager.
        return self.manager

# This is how the program starts. Basic Python start up here.
if __name__ == "__main__":
    # Telling it to run the ButtonApp class.
    SimpleAutoButtonsApp().run()
