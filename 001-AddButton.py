# Now we are going to add a button.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Button
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

# This is your main class.
class AddButtonApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        # Since we aren't doing anything with it, display a Button with text.
        return Button(text='Hello World!')

# Tell Python to start the app.
if __name__ == "__main__":
    # We are only going to run our class.
    AddButtonApp().run()
