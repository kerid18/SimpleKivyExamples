This is a repo of examples that I am building as I learn [Kivy](https://kivy.org). Hopefully it will be useful for someone else.

000-Start.py  : The most simplest example I could make.  
001-AddButton.py : Add a button that doesn't do anything.  
002-UseButton.py : Convert to Kivy Language.  
003-UseKvButton.py : Convert to Kivy Language in seperate file.  
004-ButtonAction.py : Making the Button perform and action.  
005-MultiScreenButton.py : Making multiple Buttons switch screens.  
006-PaintButton.py : Paint and Button modifications from [inclement's kivycrashcourse](https://github.com/inclement/kivycrashcourse)  
007-AutoGenButton.py : Generate Buttons based on a list programmatically.  
008-AutoGenButtonScroll.py : Same as 007, but in a scroll view for large number of items.  
009-MultiScreenAutoButton.py : Up the difficulty. Many screens. Auto generated Buttons. And Templates. Oh my!  
